STATES LIST

POSEY 404 
VANERBURGH 386
GIBSON 400
PIKE 403 
DUBOIS 384
WARRICK 397 
SPENCER 393 
PERRY 406
CRAWFORD 399 
ORANGE 402
HARRISON 398

campaignTrail_temp.states_json = JSON.parse("[{\"model\": \"campaign_trail.state\", \"pk\": 384, \"fields\": {\"name\": \"DuBois\", \"abbr\": \"CT\", \"electoral_votes\": 0, \"popular_votes\": 715, \"poll_closing_time\": 90, \"winner_take_all_flg\": 1, \"election\": 13}}, {\"model\": \"campaign_trail.state\", \"pk\": 386, \"fields\": {\"name\": \"Vanderburgh\", \"abbr\": \"GA\", \"electoral_votes\": 0, \"popular_votes\": 1192, \"poll_closing_time\": 90, \"winner_take_all_flg\": 1, \"election\": 13}}, {\"model\": \"campaign_trail.state\", \"pk\": 393, \"fields\": {\"name\": \"Spencer\", \"abbr\": \"MA\", \"electoral_votes\": 0, \"popular_votes\": 1100, \"poll_closing_time\": 90, \"winner_take_all_flg\": 1, \"election\": 13}}, {\"model\": \"campaign_trail.state\", \"pk\": 397, \"fields\": {\"name\": \"Warrick\", \"abbr\": \"NH\", \"electoral_votes\": 0, \"popular_votes\": 1145, \"poll_closing_time\": 90, \"winner_take_all_flg\": 1, \"election\": 13}}, {\"model\": \"campaign_trail.state\", \"pk\": 398, \"fields\": {\"name\": \"Harrison\", \"abbr\": \"NJ\", \"electoral_votes\": 0, \"popular_votes\": 2316, \"poll_closing_time\": 60, \"winner_take_all_flg\": 1, \"election\": 13}}, {\"model\": \"campaign_trail.state\", \"pk\": 399, \"fields\": {\"name\": \"Crawford\", \"abbr\": \"NY\", \"electoral_votes\": 0, \"popular_votes\": 917, \"poll_closing_time\": 120, \"winner_take_all_flg\": 1, \"election\": 13}}, {\"model\": \"campaign_trail.state\", \"pk\": 400, \"fields\": {\"name\": \"Gibson\", \"abbr\": \"NC\", \"electoral_votes\": 0, \"popular_votes\": 1359, \"poll_closing_time\": 60, \"winner_take_all_flg\": 1, \"election\": 13}}, {\"model\": \"campaign_trail.state\", \"pk\": 402, \"fields\": {\"name\": \"Orange\", \"abbr\": \"PA\", \"electoral_votes\": 0, \"popular_votes\": 1603, \"poll_closing_time\": 0, \"winner_take_all_flg\": 1, \"election\": 13}}, {\"model\": \"campaign_trail.state\", \"pk\": 403, \"fields\": {\"name\": \"Pike\", \"abbr\": \"RI\", \"electoral_votes\": 0, \"popular_votes\": 859, \"poll_closing_time\": 90, \"winner_take_all_flg\": 1, \"election\": 13}}, {\"model\": \"campaign_trail.state\", \"pk\": 404, \"fields\": {\"name\": \"Posey\", \"abbr\": \"SC\", \"electoral_votes\": 0, \"popular_votes\": 1643, \"poll_closing_time\": 0, \"winner_take_all_flg\": 1, \"election\": 13}}, {\"model\": \"campaign_trail.state\", \"pk\": 406, \"fields\": {\"name\": \"Perry\", \"abbr\": \"VT\", \"electoral_votes\": 0, \"popular_votes\": 818, \"poll_closing_time\": 180, \"winner_take_all_flg\": 1, \"election\": 13}}]");
